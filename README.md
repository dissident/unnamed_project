# Example of using my mapper library in fresh django project

## Installation

You need installed redis-server, because application use it as backend for celery

```
    pip3 install virtualenv
    virtualenv venv
    source venv/bin/activate
    pip install -r requirements.txt
    python manage.py migrate
    python manage.py createsuperuser
    redis-server &
    celery worker -A unknown_project --concurrency=4 &
    python manage.py runserver
```

## Usage

1. Go to `http://localhost:8000/admin/` and login as a superuser
2. On dashboard page click Map Rss Feed link
3. fill url input
4. choose adapter from adapters select
5. click submit for fetch and parse rss link
6. Choose User, Post or Tag tables for see results

## По задаче

В рамках поставленной задачи родилось следующее решение. Библиотека получилась расширяемой и гибкой. Расширяемость можно увидеть на примере RedisRssAdapter в котором мы переопределяем метод сбора данных о пользователях, потом в фиде имя возвращается как ссылка на редит аккаунт.

В дальнейшем для использования библиотеки я бы предложил реализовать следующий функционал:

- необходимо сделать возможность пост обработки данных после парсинга. Я бы сделал это через декоратор для главной функции parse для адаптера, чтобы можно было выполнить произвольный код после парсинга. Как пример строки с датами распарсить в DateTime обьекты джанги.
- при желании можно расширить список моделей или кастомизировать его вынеся обработку внутри `for entry in feed_entries:` в отдельную функцию, которую так же можно будет переопределить.
- так же добавляя новые адаптеры можно заметить недостающий функционал и реалтзовать его.

Redis и Celery были взяты всвязи с требованиями к задаче, а именно "Библиотека должна иметь высокую производительность и гарантированную доставку информации в базу."

Откуда дебильные названия для библиотеки и примера проекта? Из консперативных ограничений того же самого задания. В реальной жизни я бы их назвал Фидмапером или как то так.

Спасибо за уделенное время.
P.S. Питон стал чертовски быстр, это не может не радовать.
