from unnamed_library.adapters.rss_adapter import RssAdapter


class ReditRssAdapter(RssAdapter):
    def __authors(self, entry):
        return map(
            lambda author: {'name': self.__get_author_name(author.name)},
            entry.authors
        )

    def __get_author_name(self, name):
        return name.split('/')[-1]
