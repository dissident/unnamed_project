from django.shortcuts import render
from django.http import HttpResponseRedirect
from .tasks import parse_rss_feed


def map_rss_form(request):
    adapters = ['RssAdapter', 'ReditRssAdapter']
    return render(
        request,
        'map_rss/admin/map_rss_form.html',
        {'adapters': adapters}
    )


def map_rss_post(request):
    url = request.POST['url']
    adapter = request.POST['adapter']
    parse_rss_feed.delay(url, adapter)
    return HttpResponseRedirect('/admin')
