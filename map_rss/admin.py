from django.contrib import admin

from .models import User, Tag, Post

admin.site.index_template = "map_rss/admin/my_index.html"

admin.site.register(User)
admin.site.register(Tag)
admin.site.register(Post)
