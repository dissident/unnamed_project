from django.db import models


class User(models.Model):
    name = models.CharField(blank=False, max_length=20, unique=True)

    objects = models.Manager()

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(blank=False, max_length=20, unique=True)

    objects = models.Manager()

    def __str__(self):
        return self.name


class Post(models.Model):
    title = models.CharField(blank=False, max_length=50, unique=True)
    body = models.TextField(blank=False)
    published_at = models.DateTimeField(null=True)

    objects = models.Manager()

    def __str__(self):
        return self.title
