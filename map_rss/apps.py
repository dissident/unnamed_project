from django.apps import AppConfig


class MapRssConfig(AppConfig):
    name = 'map_rss'
