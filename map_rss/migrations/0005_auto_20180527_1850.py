# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-05-27 18:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('map_rss', '0004_auto_20180527_1842'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='published_at',
            field=models.DateTimeField(null=True),
        ),
    ]
