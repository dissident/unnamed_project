import logging
import sys

from unknown_project.celery import app
from unnamed_library.adapters.rss_adapter import RssAdapter
from map_rss.models import User, Post, Tag
from libs.custom_unnamed_library.adapters.redit_rss_adapter import ReditRssAdapter


@app.task
def parse_rss_feed(url, adapter):
    try:
        feed = globals()[adapter]()
        feed.parse(url)
        for user in feed.users:
            User.objects.update_or_create(name=user['name'])
        for tag in feed.tags:
            Tag.objects.update_or_create(name=tag['name'])
        for post in feed.posts:
            Post.objects.update_or_create(
                title=post['title'],
                body=post['body'],
            )
    except:
        logging.warning("Tried to parse feed and filed with error: '%s'" % sys.exc_info()[0])
